# Instagram Hashtags Spy 

Espía quiénes están publicando en un especifico hashtags en instagram

### Instalación en entorno ubuntu

```sh
sudosudo apt-get install python3.6
sudo apt install python3-pip
pip install instabot python-dotenv beautifultable
```
#### Instalación en entorno windows
#### Enlaces requeridos  

| Programas |Enlacesde descarga y tutoriales |
| ------ | ------ |
| Python |  [Python3.6]   |
| Moudlo pip |  [pip]   |

Despues de instalar procede a ejecutar el siguiente comando

```sh
pip install instabot python-dotenv beautifultable
# Entar al folder a la carpeta del repositorio
cd project 
mv 1.env .env
# Realiza tu primer escaneo de hashtags
python3 main.py -ht marketingdigital,marketing
```

Nota: para eviatar agregar los datos cada vez que deseas realizar un escaneo de Hashtags abre el archivo .env  y agrega los accesos de la cuenta de instagram con la cual realizaras el escaneo    

[Python3.6]: <https://www.python.org/downloads/windows/>
[Pip]: <https://recursospython.com/guias-y-manuales/instalacion-y-utilizacion-de-pip-en-windows-linux-y-os-x/>