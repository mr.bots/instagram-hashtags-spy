import os
from shutil import rmtree

class File:
  def __init__(self, file_path=""):
    self.file_path = file_path

  def isFile(self):
    return os.path.isfile(self.file_path)
  
  def isDir(self):
    return os.path.isdir(self.file_path) 
  
  def exists(self):
    return os.path.exists(self.file_path)

  def remove(self):
    if self.isFile():
      os.remove(self.file_path)
    if self.isDir():
      rmtree(self.file_path)
  
  def add(self, content="", path=""):
    if self.isFile():
      filePath = self.path()
      f = open( filePath, "a")
      f.write(content)
      f.close()
  
  def read(self):
    if self.isFile():
      filePath = self.path()
      f = open( filePath, "r")
      content = f.read()
      f.close()
      return content  
  
  def readLines(self):
    if self.isFile():
      filePath = self.path()
      f = open( filePath, "rb")
      content = f.readlines()
      f.close()
      return content 
    
  def new(self, content = ""):
    if self.isFile():
      filePath = self.path()
      f = open( filePath, "w")
      f.write(content)
      f.close()
      return content 
