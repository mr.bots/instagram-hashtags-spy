#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Hashtags Spy
    Workflow:
        Espía quiénes están publicando en un especifico hashtags en instagram
"""
import argparse
import os
import sys
from instabot import Bot
import json
from beautifultable import BeautifulTable
from utils import File as File
from utils.banner import banner  
from dotenv import load_dotenv
import time

load_dotenv()

parser = argparse.ArgumentParser(add_help=True)
parser.add_argument("-ht", type=str, help="hashtags")
args = parser.parse_args()

rootPath =   os.path.dirname(os.path.abspath(__file__))
config_folder_path = rootPath + "/config"

def instagram_spy(hashtags):

    if(len(hashtags)):

        username = os.environ.get('user')
        password = os.environ.get('pass')

        users = []

        bot = Bot()

        if(username and password):
            bot.login(username=username, password=password)
        else:
            bot.login()

        table = BeautifulTable()
        table.columns.header = ["Username", "Name","Email","Followers", 'Following', 'Posts', 'Hashtag']

        for hashtag in hashtags:

            print('')
            print('Información para el hastag', hashtag)

            users = bot.get_hashtag_users(hashtag.rstrip())
            
            sub_total_followers = 0
            sub_total_users = 0
            sub_total_following = 0
            sub_total_media = 0
            
            for user in users:
                user_data = bot.get_user_info(user)
                
                if(user_data):  
                  public_email = 'Email not found' if not('public_email' in  user_data) else user_data['public_email']
                  full_name = 'Name no found' if not('full_name' in  user_data) else user_data['full_name']
                  username = user_data['username']
                  follower_count = user_data['follower_count']
                  following_count = user_data['following_count']
                  media_count = user_data['media_count']
                 
                  sub_total_users = sub_total_users + 1
                  sub_total_followers = sub_total_followers + follower_count
                  sub_total_following = sub_total_following + following_count
                  sub_total_media = sub_total_media + media_count
                  
                  table.rows.append([ username, full_name, public_email, follower_count,  following_count, media_count, hashtag ])
            
            table.rows.append([ sub_total_users, '', '', sub_total_followers,  sub_total_following, sub_total_media, '' ])
            print(table)

def main():

    if(not args.ht):
        print('No se hallaron hashtags')
        print('Ejemplo: python3 main.py -ht marketingdigital,marketing')
        return
      
    
    hashtags = args.ht.split(",")

    if(not len(hashtags)):
        print('No se hallaron hashtags')
        return

    config_folder = File.File(config_folder_path)

    if(config_folder.isDir()):
        config_folder.remove()
        time.sleep(2)

    banner()

    instagram_spy(hashtags)
    

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
         sys.exit(0)